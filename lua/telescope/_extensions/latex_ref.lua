local has_telescope, telescope = pcall(require, "telescope")
if not has_telescope then
  error(
    "This plugin requires telescope.nvim (https://github.com/nvim-telescope/telescope.nvim)"
  )
end

local latex_ref_picker = require("telescope-latex-ref").picker

return telescope.register_extension({
  exports = {
    latex_ref = latex_ref_picker,
  },
})
