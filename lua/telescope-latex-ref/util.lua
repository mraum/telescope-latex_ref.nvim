local insert_unique = function (as, a)
  for _, b in pairs(as) do
    if a == b then
      return
    end
  end
  table.insert(as, a)
end

return {
  insert_unique = insert_unique,
}
