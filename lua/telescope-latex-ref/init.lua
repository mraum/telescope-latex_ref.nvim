local plenary_scan = require("plenary.scandir")
local plenary_path = require("plenary.path")

local parsers = require("nvim-treesitter.parsers")
local query = require("nvim-treesitter.query")

local actions = require("telescope.actions")
local finders = require("telescope.finders")
local pickers = require("telescope.pickers")
local state = require("telescope.state")
local telescope_config = require("telescope.config").values

local insert = require("telescope-latex-ref.insert")
local util = require("telescope-latex-ref.util")


local _tex_files = {}
local _init_tex_files = false

local populate_tex_files = function (dir, tex_files)
  plenary_scan.scan_dir(dir, {
    depth = 1,
    search_pattern = '.*%.tex',
    on_insert = function(file)
      util.insert_unique(tex_files, plenary_path:new(file))
    end,
  })
end

local with_tex_files = function (on_file)
  if not _init_tex_files then
    populate_tex_files(".", _tex_files)
    _init_tex_files = true
  end
  for _,path in ipairs(_tex_files) do
    on_file(path)
  end
end

local populate_labels_from_file = function (path, bufnr, labels)
  if not path:exists() then
    return {}
  end
  for lnum,l in ipairs(vim.split(path:read(), "\n", {})) do
    for label in string.gmatch(l, "%\\label{([^\n}]*)}") do
      table.insert(labels,
      {
        src_bufnr = bufnr,
        string = label,
        filename = path:absolute(),
        lnum = lnum,
      })
    end
  end
end

local populate_labels_from_treesitter = function (bufnr, labels)
  if not query.has_query_files("latex", "telescope-latex-ref") then
    error("No treesitter query file for telescope-latex-ref")
  end

  local parser = parsers.get_parser(bufnr)
  local syntax_tree = parser:parse()[1]
  local filename = vim.api.nvim_buf_get_name(bufnr)

  for match in query.iter_group_results(bufnr, "telescope-latex-ref", syntax_tree:root(), "latex") do
    local name_node = match.name.node
    local row, _ = name_node:start()
    table.insert(labels,
    {
      node = match.label.node,
      src_bufnr = bufnr,
      string = vim.treesitter.get_node_text(name_node, bufnr),
      filename = filename,
      lnum = row,
    })
  end
end

local toggel_preview_wrapped = function (prompt_bufnr)
  local status = state.get_status(prompt_bufnr)
  local preview_winid = status.layout.preview.winid
  local is_wrapped = vim.api.nvim_get_option_value("wrap", {scope = "local", win = preview_winid})
  vim.api.nvim_set_option_value("wrap", not is_wrapped, {scope = "local", win = preview_winid})
end

local picker = function (opts)
  local labels = {}
  if opts.treesitter then
    populate_labels_from_treesitter(opts.bufnr, labels)
  else
    with_tex_files(function (path)
      populate_labels_from_file(path, opts.bufnr, labels)
    end)
  end

  pickers
  .new(opts, {
    prompt_title = "Latex Labels",
    finder = finders.new_table({
      results = labels,
      entry_maker = function (label)
        return {
          value   = label,
          ordinal = label.string,
          display = label.string,
          -- preview
          filename = label.filename,
          lnum = label.lnum,
        }
      end,
    }),
    sorter = telescope_config.generic_sorter(opts),
    previewer = telescope_config.grep_previewer(opts),
    attach_mappings = function (_, map)
      actions.select_default:replace(insert.insert_auto_reference)
      map("i", "<C-r>", insert.insert_plain_reference)
      map("i", "<C-e>", insert.insert_equation_reference)
      map("i", "<C-t>", insert.insert_page_reference)
      map("i", "<C-f>", insert.insert_full_reference)
      map("i", "<C-w>", toggel_preview_wrapped)
      map("n", "r", insert.insert_plain_reference)
      map("n", "e", insert.insert_equation_reference)
      map("n", "t", insert.insert_page_reference)
      map("n", "f", insert.insert_full_reference)
      map("n", "w", toggel_preview_wrapped)
      return true
    end,
  })
  :find()
end

return {
  picker = picker,
}
