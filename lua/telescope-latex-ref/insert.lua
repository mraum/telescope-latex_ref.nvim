local actions = require("telescope.actions")
local actions_state = require("telescope.actions.state")

local insert_format_string = function(prompt_bufnr, format_string)
  local label = actions_state.get_selected_entry().value.string
  local str = string.format(format_string, label)
  actions.close(prompt_bufnr)
  vim.api.nvim_put({str}, "", false, true)
  vim.api.nvim_feedkeys('a', 'n', true)
end

local insert_named_reference = function (prompt_bufnr, name)
  insert_format_string(prompt_bufnr, string.format("%s~\\ref{%s}", name, "%s"))
end

local insert_named_equation_reference = function (prompt_bufnr, name)
  insert_format_string(prompt_bufnr, string.format("%s~\\eqref{%s}", name, "%s"))
end

local insert_plain_reference = function (prompt_bufnr)
  insert_format_string(prompt_bufnr, "\\ref{%s}")
end

local insert_equation_reference = function (prompt_bufnr)
  insert_format_string(prompt_bufnr, "\\eqref{%s}")
end

local insert_page_reference = function (prompt_bufnr)
  insert_format_string(prompt_bufnr, "\\pageref{%s}")
end

local insert_auto_reference = function (prompt_bufnr)
  local value = actions_state.get_selected_entry().value
  local node = value.node
  if node then
    if node:parent():type() == "math_environment" then
      insert_equation_reference(prompt_bufnr)
    else
      insert_plain_reference(prompt_bufnr)
    end
  else
    if string.sub(value.string,1,3) == "eq:" then
      insert_equation_reference(prompt_bufnr)
    else
      insert_plain_reference(prompt_bufnr)
    end
  end
end

local insert_full_reference = function (prompt_bufnr)
  local value = actions_state.get_selected_entry().value
  local node = value.node
  if node then
    local parent = node:parent()
    local type = parent:type()

    if type == "part" then
      insert_named_reference(prompt_bufnr, "Part")
    elseif type == "Chapter" then
      insert_named_reference(prompt_bufnr, "Chapter")
    elseif type == "section" or type == "subsection" or type == "subsubsection" then
      insert_named_reference(prompt_bufnr, "Section")
    elseif type == "paragraph" or type == "subparagraph" then
      insert_named_reference(prompt_bufnr, "Paragraph")

    elseif type == "math_environment" then
      insert_named_equation_reference(prompt_bufnr, "Equation")

    elseif type == "generic_environment" then
      local src_bufnr = actions_state.get_selected_entry().value.src_bufnr
      local environment_text = parent:field("begin")[1]:field("name")[1]:field("text")[1]
      local environment = vim.treesitter.get_node_text(environment_text, src_bufnr)
      if environment == "claim" then
        insert_named_reference(prompt_bufnr, "Claim")
      elseif environment == "conjecture" then
        insert_named_reference(prompt_bufnr, "Conjecture")
      elseif environment == "corollary" or environment == "maincorollary" then
        insert_named_reference(prompt_bufnr, "Corollary")
      elseif environment == "lemma" then
        insert_named_reference(prompt_bufnr, "Lemma")
      elseif environment == "problem" then
        insert_named_reference(prompt_bufnr, "Problem")
      elseif environment == "proposition" then
        insert_named_reference(prompt_bufnr, "Proposition")
      elseif environment == "theorem" or environment == "maintheorem" then
        insert_named_reference(prompt_bufnr, "Theorem")

      elseif environment == "assumption" then
        insert_named_reference(prompt_bufnr, "Assumption")
      elseif environment == "axiom" then
        insert_named_reference(prompt_bufnr, "Axiom")
      elseif environment == "definition" then
        insert_named_reference(prompt_bufnr, "Definition")
      elseif environment == "example" then
        insert_named_reference(prompt_bufnr, "Example")
      elseif environment == "exercise" then
        insert_named_reference(prompt_bufnr, "Exercise")
      elseif environment == "notation" then
        insert_named_reference(prompt_bufnr, "Notation")

      elseif environment == "remark" or environment == "remarks" then
        insert_named_reference(prompt_bufnr, "Remark")
      end

    else
      insert_plain_reference(prompt_bufnr)
    end
  else
    local prefix = string.match(value.string, "([^:]*):")
    print("prefix " .. prefix)

    if prefix == "part" then
      insert_named_reference(prompt_bufnr, "Part")
    elseif prefix == "ch" then
      insert_named_reference(prompt_bufnr, "Chapter")
    elseif prefix == "sec" or prefix == "ssec" or prefix == "sssec" then
      insert_named_reference(prompt_bufnr, "Section")
    elseif prefix == "par" or prefix == "spar" then
      insert_named_reference(prompt_bufnr, "Paragraph")

    elseif prefix == "eq" then
      insert_named_equation_reference(prompt_bufnr, "Equation")

    elseif prefix == "claim" then
      insert_named_reference(prompt_bufnr, "Claim")
    elseif prefix == "conj" then
      insert_named_reference(prompt_bufnr, "Conjecture")
    elseif prefix == "cor" or prefix == "maincor" then
      insert_named_reference(prompt_bufnr, "Corollary")
    elseif prefix == "la" then
      insert_named_reference(prompt_bufnr, "Lemma")
    elseif prefix == "prblm" then
      insert_named_reference(prompt_bufnr, "Problem")
    elseif prefix == "prop" then
      insert_named_reference(prompt_bufnr, "Proposition")
    elseif prefix == "thm" or prefix == "mainthm" then
      insert_named_reference(prompt_bufnr, "Theorem")

    elseif prefix == "assmpt" then
      insert_named_reference(prompt_bufnr, "Assumption")
    elseif prefix == "axiom" then
      insert_named_reference(prompt_bufnr, "Axiom")
    elseif prefix == "def" then
      insert_named_reference(prompt_bufnr, "Definition")
    elseif prefix == "ex" then
      insert_named_reference(prompt_bufnr, "Example")
    elseif prefix == "exc" then
      insert_named_reference(prompt_bufnr, "Exercise")
    elseif prefix == "not" then
      insert_named_reference(prompt_bufnr, "Notation")

    elseif prefix == "rm" then
      insert_named_reference(prompt_bufnr, "Remark")

    else
      insert_plain_reference(prompt_bufnr)
    end
  end
end


return {
  insert_plain_reference          = insert_plain_reference,
  insert_equation_reference       = insert_equation_reference,
  insert_page_reference           = insert_page_reference,
  insert_auto_reference           = insert_auto_reference,
  insert_full_reference           = insert_full_reference,
}
